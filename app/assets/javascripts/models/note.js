EN.Models.Note = Backbone.Model.extend({
  urlRoot: '/notes',
});

EN.Models.Note.changeNotebook = function(noteId, notebookToId, nbTitle) {
  var note = new EN.Models.Note({id: noteId});
  note.set('notebook_id', notebookToId);
  console.log("in Note.changeNotebook before save")
  note.save(null, {
    success: function() {
      console.log("in notesave's (in changeNotebook method) success callback ");
      eventFirer.trigger("modifyViews", noteId, nbTitle);
    },
    error: function () {
      console.log("note move unsuccessful");
    }
  });
  console.log("after note.save()");
}
