EN.Views.TagsIndexView = Backbone.View.extend({
  tagName: 'ul',
  className: 'tags-list',
  template: JST['tags/index'],
  initialize: function() {
    var self = this;
    var renderCallback = self.render.bind(self);
    self.listenTo(self.collection, "change", renderCallback);
    self.listenTo(self.collection, "destroy", renderCallback);
  },
  events: {
    "click .delete": "deleteTag",
    "click .rename": "renameTag",
    "click .tag-list-item": "displayTagged"
  },
  displayTagged: function () {
    event.preventDefault();
    var id = $(event.target).attr('data-id');
    console.log("in event handler")
    var taggedNotes = new EN.Collections.Notes;
    taggedNotes.fetch({
      url: '/tags/' + id,
      success: function () {
        var taggedNotesView = new EN.Views.NbShow({collection: taggedNotes});
        console.log(taggedNotes)
        $('#notes-sidebar').html(taggedNotesView.render().$el);
      },
      error: function () {
        console.log("error in fetching tagged notes")
      }
  })},
  render: function () {
    this.$el.empty();
    var self = this;
    this.$el.append("<h3>Tags</h3>")
    this.collection.each(function(tag) {
      self.$el.append(self.template({tag: tag}));
    })

    return self;
  },
  deleteTag: function () {
    var tagId = $(event.target).closest(".tag-list-item").attr('data-id');
    var tag = this.collection.get(tagId);
    tag.destroy();
  },
  renameTag: function () {
    var tagId = $(event.target).closest(".tag-list-item").attr('data-id');
    var tag = this.collection.get(tagId);
    var tagName = prompt("New Tag Name", tag.get('title'));
    tag.set('title', tagName);
    tag.save();
  }
})