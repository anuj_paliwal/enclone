EN.Views.NotebooksIndex = Backbone.View.extend ({
  tagName: 'ul',
  className: 'notebooks-list',
  template: JST['notebooks/index'],
  initialize: function () {
    var self = this;
    var renderCallback = self.render.bind(self);
    self.listenTo(self.collection, 'change', renderCallback);
    self.listenTo(self.collection, 'destroy', renderCallback);
  },
  events: {
    "click .delete-button": "deleteNotebook",
    "click .rename": "renameNotebook",
    "click .notebook-list-item": "changeNotebook"
  },
  changeNotebook: function () {
    var notebookId = $(event.target).attr('data-notebookId');
    var notebookTitle = $(event.target).attr('data-notebookTitle');
    currentNotebook = {id: notebookId, title: notebookTitle};
    //eventFirer.trigger("notebookChange");
  },
  render: function() {
    this.$el.empty();
    var self = this;
    this.$el.append("<h3>Notebooks</h3>");
    this.collection.each(function(notebook){
      var $renderedNotebook = $(self.template({notebook: notebook}))
      $renderedNotebook.draggable({revert: true});
      $renderedNotebook.droppable({
        drop: function (event, ui) {
          //try to pass collection into draggable attribute
          var noteId = $(ui.draggable.find('a')[0]).attr('data-id');
          var nbTitle = $(ui.draggable.find('a')[0]).attr('data-nbTitle');
          //console.log(self.collection.get(noteId));
          EN.Models.Note.changeNotebook(noteId, notebook.get('id'), nbTitle);
          }
        });
      self.$el.append($renderedNotebook)
    });

    return this;
  },
  deleteNotebook: function() {
    var self = this;
    event.preventDefault();
    var notebookLi = $(event.target).closest('.notebook-list-item');
    var id = notebookLi.attr('data-id');
    var notebook = this.collection.get(id);
    notebook.destroy({
      success: function () {
        var differentNb = self.collection.first();
        var notes = new EN.Collections.Notes;
        notes.fetch({
          url: 'notebooks/' + differentNb.get('id'),
          success: function() {
            var noteListView = new EN.Views.NbShow({
              collection: notes,
              title: differentNb.get('title')
            });
            var $notesSideBarEl = $("#notes-sidebar");
            $notesSideBarEl.html(noteListView.render().$el);
          },
          error: function() {
            console.log("Notes fetch for notebook was unsuccessful");
          }
        })
      },
      error: function () {
        console.log("error");
      }
    });
  },
  renameNotebook: function() {
    event.preventDefault();
    var notebookLi = $(event.target).closest('.notebook-list-item');
    var id = notebookLi.attr('data-id');
    var notebook = this.collection.get(id);
    var name = prompt("Notebook name?", notebook.get('title'));
    notebook.fetch({
      success: function (resp, coll) {
        notebook.set('title', name);
        notebook.save({
          success: function() {

          }
        });
      },
      error: function () {
        console.log("Notebook fetch unsucessful")
      }
    })
  }
})


// {
//     event.stopPropagation();
//     console.log('debugging line')
//     var notebook = this.collection.get(id);
//     var name = prompt("Notebook name?", notebook.get('title'));
//
//     notebook.fetch({
//       success: function (resp, coll) {
//         notebook.set('title', name);
//         notebook.save();
//         console.log("success callback")
//       },
//       error: function () {
//         console.log("Notebook fetch unsucessful")
//       }
//     })
//   }