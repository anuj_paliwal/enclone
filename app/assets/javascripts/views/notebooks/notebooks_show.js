EN.Views.NbShow = Backbone.View.extend({
  tagName: "ul",
  className: "notes-list",
  template: JST['notebooks/show'],
  events: {
    "click li": "clickHandler",
    "contextmenu li": "deletePrompt"
  },
  deletePrompt: function () {
    var choice = confirm("Do you want to delete this Note?");
    if (choice) {
      var note = new EN.Models.Note({id: $(event.target).attr('data-id')})
      note.destroy({
        success: function () {
          Backbone.history.navigate("");
        },
        error: function () {
          console.log("you can't even delete a note")
        }
      }
      )
    }
  },
  initialize: function (options) {
    this.$el.empty();
    this.nbTitle = options.title;
    var self = this;
    var renderCallback = self.render.bind(self);
    self.listenTo(eventFirer, 'modifyViews', renderCallback);
    self.listenTo(self.collection, 'change', renderCallback);
    self.listenTo(self.collection, 'notebookChange', renderCallback);
  },
  render: function(id, title) {
    if (typeof id !== "undefined") {
      var removeThis = this.collection.get(id);
      this.collection.remove(removeThis);
    }

    var self = this;
    this.$el.empty();
    $header = $("<h3></h3");
    $header.text(self.nbTitle)    //(self.nbTitle);
    this.$el.append($header);
    this.collection.each(function(note){
      //must render only one element (li) b/c of draggable
      var $renderedNoteInfo = $(self.template({
        note: note,
        title: title
      }));
      $renderedNoteInfo.draggable({revert: true, opacity: 0.3});
      self.$el.append($renderedNoteInfo);
    })

    return self;
  },
  clickHandler: function () {
    $(event.target).addClass('selected-li') //highlight
    //get info on particular note
    // $(event.target)
    var noteId = $(event.target).attr('data-id');
    var noteTitle = $(event.target).attr('data-notetitle');
    var note = this.collection.get(noteId);
    var noteDetailView = new EN.Views.NoteShowView({
      model: note});

  }
});