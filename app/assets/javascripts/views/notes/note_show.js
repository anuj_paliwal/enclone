EN.Views.NoteShowView = Backbone.View.extend({
  initialize: function() {
    var self = this;
    var renderCallback = self.render.bind(self);
    //self.listenTo(eventFirer, "nbClick", renderCallback);
    self.listenTo(eventFirer, "notebookChange", renderCallback);
  },
  template: JST['notes/show'],
  events: {
    "blur .edit-note-area": "saveNote",
    "click .create-button": "createNote"

  },
  createNote: function () {
    var note = new EN.Models.Note({
      notebook_id: currentNotebook.id
    })
    note.save({
      success: function() {
        var noteDetailView = new EN.Views.NoteShowView({model: note})
        $("#note-creation-area").html(noteDetailView.render().$el);
        Backbone.navigate('notes/' + note.get('id'));
      },
      error: function () {
        console.log("something went wrong when creating a new note.")
      }
    })
  },

  notebookSwitch: function () {
    this.$el.empty();
  },
  render: function (arg) {
    var self = this;
    console.log(currentNotebook)
    if (typeof arg !== "undefined") {
      var notebookId = arg;
      var notes = new EN.Collections.Notes;//notes returned by hitting /notebooks/arg
      notes.fetch({
        success: function () {
          self.$el.html(self.template({note: note}));
          return this;
        }
      })
    } else {
      var note = this.model;
      this.$el.html(self.template({note: note}));
      return this;
    }
  },
  saveNote: function () {
    var noteId = $(event.currentTarget).attr('data-id');
    this.model.set('body', $(event.target).val());
    this.model.save();
  }
})