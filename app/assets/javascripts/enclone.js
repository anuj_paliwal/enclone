window.EN = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function () {

    eventFirer = _.extend({}, Backbone.Events)
    var NbCollection = new EN.Collections.NbCollection;
    var tagCollection = new EN.Collections.Tags;
    var $noteBooksEl = $("#notebooks-sidebar");
    var $notesSideBarEl = $("#notes-sidebar");
    var $noteCreationArea = $("#note-creation-area");
    NbCollection.fetch({
      success: function(collection, response) {
        var router = new EN.Routers.EnRouter({
          $noteBooksEl: $noteBooksEl,
          $notesSideBarEl: $notesSideBarEl,
          $noteCreationArea: $noteCreationArea,
          notebooks: NbCollection
        });

        //console.log($notesSideBarEl)
        Backbone.history.start();
        //router.navigate("", {trigger: true});
      },
      error: function() {
        console.log("fetch unsuccessful")
      }
    })
  }
}

$(document).ready(function () {
  EN.initialize();
})