EN.Routers.EnRouter = Backbone.Router.extend({
  initialize: function(options) {
    this.$noteBooksEl = options.$noteBooksEl;
    this.notebooks = options.notebooks;
    this.$notesSideBarEl = options.$notesSideBarEl;
    this.$noteCreationArea = options.$noteCreationArea;
  },
  routes: {
    "": "home",
    "notebooks/:id/:title": "show",
    "tags/:id": "tagShow",
    "notes/:id": "noteShow",
    "notebooks/edit/:id": "renameNotebook"
  },
  home: function() {
    var self = this;
    var notebooksView = new EN.Views.NotebooksIndex({collection: self.notebooks});
    this.$noteBooksEl.html(notebooksView.render().$el);
    var tagsCollection = new EN.Collections.Tags;
    var id = self.notebooks.models[0].get('id');
    var title = self.notebooks.get(id).get('title');
    currentNotebook = {title: title, id: id}
    self.show(id, title.split(' ').join('.'));
    tagsCollection.fetch({
      success: function() {
        var tagsListView = new EN.Views.TagsIndexView({collection: tagsCollection});
        self.$noteBooksEl.append(tagsListView.render().$el);

      },
      error: function() {
        console.log("fetch was unsuccessful");
      }
    });

  },
  show: function (id, title) {
    var self = this;
    var title = title.split('.').join(' ');
    currentNotebook.id = id;
    currentNotebook.title = title
    var notesCollection = new EN.Collections.Notes;

    notesCollection.fetch({
      url: 'notebooks/' + id,
      success: function() {
        var noteListView = new EN.Views.NbShow({
          collection: notesCollection,
          title: title
        });
        self.$notesSideBarEl.html(noteListView.render().$el);
        eventFirer.trigger('notebookChange', id, title);
        var note = notesCollection.first();
        var noteDetailView = new EN.Views.NoteShowView({model: note});
        $("#note-creation-area").html(noteDetailView.render().$el);

      },
      error: function() {
        console.log("Notes fetch for notebook was unsuccessful");
      }
    })
  },
  tagShow: function(id) {
    var self = this;
    var taggedNotes = new EN.Collections.Notes;
    taggedNotes.fetch({
      url: 'tags/' + id,
      success: function(col, resp) {
        var taggedNotesView = new EN.Views.NbShow({collection: taggedNotes});
        self.$notesSideBarEl.html(taggedNotesView.render().$el);
      },
      error: function() {
        console.log("failed to fetch notes for this tag")
      }
    })
  },
  noteShow: function(id) {
    var self = this;
    var note = new EN.Models.Note({id: id});
    note.fetch({
      success: function() {
        console.log("in note.fetch's success callback")
        var noteView = new EN.Views.NoteShowView({model: note});
        self.$noteCreationArea.html(noteView.render().$el);
      },
      error: function() {
        console.log("note fetch was unsuccessful")
      }
    })
  },
  removeNotebook: function(id) {
    var notebook = this.notebooks.get(id)
  }
})