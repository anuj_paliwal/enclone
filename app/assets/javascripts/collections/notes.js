EN.Collections.Notes = Backbone.Collection.extend({
  model: EN.Models.Note,
  url: '/notes'
});