EN.Collections.Tags = Backbone.Collection.extend({
  model: EN.Models.Tag,
  url: '/tags'
})