EN.Collections.NbCollection = Backbone.Collection.extend({
  url: '/notebooks',
  model: EN.Models.Notebook
})