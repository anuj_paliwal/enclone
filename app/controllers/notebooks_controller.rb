class NotebooksController < ApplicationController
  before_filter :require_current_user!
  respond_to :json, :html

  def index
    # if current_user.notebooks.any?
#       @notebook = current_user.notebooks.first
#       #@note = @notebook.notes.first if @notebook.notes.any?
#     else
#       @notebook = Notebook.create(
#       :user_id => current_user.id,
#       :title => "Default"
#       )
#       #@note = Note.new
#     end
#     @notes = @notebook.notes
    @notebooks = current_user.notebooks
    respond_with(@notebooks)

  end

  def create
    @notebook = Notebook.new(params[:notebook])
    @notebook.user_id = current_user.id
    @notes = @notebook.notes
    unless @notebook.save
      flash[:errors] = @notebook.errors.full_messages
    end

    render 'notebooks/index'
  end

  def show
    @notebook = Notebook.find(params[:id])
    @notes = @notebook.notes

    respond_with(@notes)
  end

  def new
  end

  def update
    @notebook = Notebook.find(params[:id])
    @notebook.update_attributes(params[:notebook])

    render :json => @notebook
  end

  def destroy
    @notebook = Notebook.find(params[:id])
    if current_user.notebooks.length == 1
      flash[:errors] = ["You must have at least one notebook"]
    else
      @notebook.destroy
      @notebook = current_user.notebooks.last
    end

    render :json => {}
  end

end
