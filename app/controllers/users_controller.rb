class UsersController < ApplicationController

  def new
    return redirect_to notebooks_url if logged_in?
    @user = User.new
    render :new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      send_welcome_email(@user)
      self.current_user = @user
      Notebook.create(:user_id => @user.id, :title => "Untitled")

      redirect_to root_url
    else
      flash[:errors] = @user.errors.full_messages
      redirect_to new_user_url
    end
  end

  def edit
    @user = User.find(params[:id])
    render :edit
  end

  def show
    @user = User.find(params[:id])
    redirect_to notebooks_url
    #render :show
  end

  def update
    user = User.find(params[:id])
    user.update_attributes(params[:user])
    if user.save
      redirect_to user_url
    else
      flash[:errors] = user.errors.full_messages
      redirect_to edit_user_url
    end
  end

  def destroy
    User.find(params[:id]).destroy
  end

  def send_welcome_email(user)
    msg = UserMailer.welcome_email(user, user_url(user))
    msg.deliver!
  end

end
