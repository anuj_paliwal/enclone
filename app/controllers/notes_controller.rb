class NotesController < ApplicationController
  before_filter :require_current_user!
  respond_to :html, :json

  def new
    if params[:notebook_id]
      @notebook = Notebook.find(params[:notebook_id])
    else
      @notebook = Notebook.create(
      :user_id => current_user_id,
      :title => "Default"
      )
    end

    @note = Note.create(
    :notebook_id => @notebook.id,
    :body => "",
    :title => "Untitled"
    )
    @notes = @notebook.notes
    render 'notebooks/index'
  end

  def index
    @notes = current_user.notes
    respond_with(@notes)
  end

  def create
    @note = Note.new(params[:note])
    #@notebook = Notebook.find(params[:note][:notebook_id])
    #@notes = @notebook.notes
    @note.title = "Untitled"
    @note.save!
    flash[:errors] = @note.errors.full_messages unless @note.save

    render :json => @note
  end

  def show
    # @note = Note.find(params[:id])
#     @notebook = Notebook.find(@note.notebook_id)
#     @notes = @notebook.notes
#
#     render 'notebooks/index'
    @note = Note.find(params[:id])
    respond_with(@note)
  end

  def update
    @note = Note.find(params[:id])
    @note.update_attributes(params[:note])
    @notebook = Notebook.find(@note.notebook_id)
    @notes = @notebook.notes
    # if params[:tag][:title].length > 0
#       Tag.create(
#         :user_id => current_user.id,
#         :note_id => @note.id,
#         :title => params[:tag][:title]
#       )
#     end

    render :json => @note
  end

  def destroy
    unwanted_note = Note.find(params[:id])
    @notebook = unwanted_note.notebook
    unwanted_note.destroy
    @notes = @notebook.notes
    # @note = Note.create(
#       :notebook_id => @notebook.id,
#       :title => "Untitled",
#     )

    render 'notebooks/index'
  end

end
