class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by_credentials(
      params[:user][:email],
      params[:user][:password]
    )
    if user.nil?
      flash[:errors] = ["Invalid Username or Password"]
      redirect_to new_session_url
    else
      user.reset_session_token!
      session[:session_token] = user.session_token
      redirect_to root_url
    end

  end

  def destroy
    user = User.find_by_session_token(session[:session_token])
    user.reset_session_token!
    session[:session_token] = nil
    redirect_to new_session_url
  end

end
