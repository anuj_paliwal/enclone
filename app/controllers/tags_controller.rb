class TagsController < ApplicationController
  respond_to :html, :json
  before_filter :require_current_user!
  def index
    @tags = current_user.tags
    respond_with(@tags)
  end

  def create
    # #if tag (tag with desired title) doesn't exist create a new tagging
#     #if tag (title) does exist, add a note to the tag or a tag to the note

    @tag = Tag.new(params[:tag])
    @tag.user_id = current_user.id
    tag_name = params[:tag][:title].downcase
    params[:tag][:title] = tag_name
#
    @note = Note.find(params[:tag][:note_ids].first.to_i) #assume 1-element array
    @notebook = Notebook.find(@note.notebook_id)
    @notes = @notebook.notes
    if Tag.exists?(:title => tag_name) && @note.tag_names.include?(tag_name)
      flash[:errors] = ["Note already has this tag"]
    elsif Tag.exists?(:title => tag_name)
      @tag = Tag.find_by_title(tag_name)
      Tagging.create(:note_id => @note.id, :tag_id => @tag.id)
    else
      @tag.save
    end

   render 'notebooks/index'
  end

  def destroy
    Tag.find(params[:id]).destroy
    render :json => "Tag deleted"
  end

  def show
    tag = Tag.find(params[:id])
    #tagged_note_ids = Tag.select("note_id").where(:title => title)
    @notebook = current_user.notebooks.first #should probly make it all notes
    @notes = tag.notes

    respond_with(@notes)
  end

end
