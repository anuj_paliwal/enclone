class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def welcome_email(user, show_route)
    @user = user
    @url = show_route #w/ registration token?
    mail(to: user.email, subject: "Welcome to Enclone")
  end

end
