class Tagging < ActiveRecord::Base
  attr_accessible :note_id, :tag_id
  validates :note_id, :tag_id, :presence => true
  validates_uniqueness_of :note_id, :scope => :tag_id

  belongs_to :note
  belongs_to :tag
end
