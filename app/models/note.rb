class Note < ActiveRecord::Base
  attr_accessible :title, :body, :notebook_id, :tag_ids
  validates :notebook_id, :presence => true

  belongs_to :notebook
  #has_many :tags, dependent: :destroy
  belongs_to :user
  has_many :taggings
  has_many :tags, :through => :taggings

  def tag_names
    self.tags.map { |tag| tag.title }
  end

end
