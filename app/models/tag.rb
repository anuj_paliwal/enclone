class Tag < ActiveRecord::Base
  attr_accessible :title, :tagging_id, :user_id, :note_ids
  validates :title, :user_id, :presence => true
  validates_uniqueness_of :title#, scope: :note_id

  belongs_to :user
  has_many :taggings
  has_many :notes, :through => :taggings
  #belongs_to :note
end
