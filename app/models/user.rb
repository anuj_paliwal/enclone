class User < ActiveRecord::Base
  attr_accessible :password, :email, :session_token
  attr_reader :password

  before_validation :ensure_session_token
  validates :password_digest, :session_token, :email, :presence => true
  validates :email, :uniqueness => true

  has_many :notebooks
  has_many :tags
  has_many :notes, through: :notebooks

  def self.find_by_credentials(email, password)
    user = User.find_by_email(email)
    return nil if user.nil?
    user.is_password?(password) ? user : nil
  end

  def reset_session_token
    self.session_token = SecureRandom.urlsafe_base64(16)
    self.save
    return self.session_token
  end

  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password).to_s
  end

  def ensure_session_token
    self.session_token ||= reset_session_token
  end

end