Enclone::Application.routes.draw do
  resources :users, :except => [:index]
  resource :session, :only => [:new, :create, :destroy]
  resources :notebooks
  resources :notes
  resources :tags, :only => [:create, :index, :destroy, :show]

  root :to => "root#root"
end