class ChangeNotebookIdColumnToNoteIdInTags < ActiveRecord::Migration
  def change
   rename_column :tags, :notebook_id, :note_id
   add_column :tags, :title, :string
  end
end
