class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.integer :notebook_id, :null => false

      t.timestamps
    end

    add_index :tags, :notebook_id
  end
end
