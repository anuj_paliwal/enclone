class RemoveNoteIdColumnFromTagTable < ActiveRecord::Migration
  def change
    remove_column :tags, :note_id
  end
end
