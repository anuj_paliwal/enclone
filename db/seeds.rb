# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(
  :email => "anu",
  :password => "password"
)
User.create(
  :email => "other_user",
  :password => "password"
)

anu = Notebook.create(
  :user_id => User.first.id,
  :title => "anu's notebook 1"
)

note1 = Note.create(
  :notebook_id => Notebook.find_by_title("anu's notebook 1").id,
  :title => "anu's note 1",
  :body => "Note 1's body"
)

n2 = Notebook.create(
  :user_id => User.first.id,
  :title => "anu's notebook 2"
)

n3 = Notebook.create(
  :user_id => User.first.id,
  :title => "anu's notebook 3"
)

n4 = Notebook.create(
  :user_id => User.first.id,
  :title => "anu's notebook 4"
)

n5 = Notebook.create(
  :user_id => User.first.id,
  :title => "anu's notebook 5"
)


note2 = Note.create(
  :notebook_id => n2.id,
  :title => "notebook-2-note-1",
  :body => "body is not null"
)

note3 = Note.create(
  :notebook_id => n2.id,
  :title => "notebook-2-note-2",
  :body => "body is really not null"
)

t = Tag.create(:title => "Tag", :user_id => anu.id)
t2 = Tag.create(:title => "Taggy", :user_id => anu.id)
t3 = Tag.create(:title => "Taggerifically Taggy", :user_id => anu.id)
t4 = Tag.create(:title => "Taggilicious", :user_id => anu.id)

t2.note_ids = [note1.id]
t3.note_ids = [note2.id]
t4.note_ids = [note1.id, note2.id]

t.note_ids = [note1.id, note2.id]
